"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment 
MailChimp Writer: Users will have to provide the email template from mailchimp to run this
"""

from pygelf import GelfTcpHandler
import sys
import os
import logging
import csv
import json
import pandas as pd
from mailchimp3 import MailChimp
from keboola import docker
import time


### Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

### Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"

### Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
USERNAME = cfg.get_parameters()["username"]
API_KEY = cfg.get_parameters()["#api_key"]
FROM_NAME = cfg.get_parameters()["from_name"]
FROM_EMAIL = cfg.get_parameters()["from_email"]
TEMPLATE_ID = cfg.get_parameters()["template_id"]
template_update = cfg.get_parameters()["template_update"]

if template_update == "YES":
    TEMPLATE_UPDATE = True
    logging.info("Template update mode: Enabled.")
else:
    TEMPLATE_UPDATE = False

"""
Sample Config
{
    "username": "",
    "#api_key": "",
    "from_name": "leo",
    "from_email": "leo@keboola.com",
    "template_id": "",
    "template_update":true/false,
    "debug": false
}
"""

client = MailChimp(USERNAME,API_KEY)

### Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
logging.info("IN tables mapped: "+str(in_tables))


def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """
    ### input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    if not TEMPLATE_UPDATE:
        return in_name
    else:
        table = in_tables[1]
        in_name_1 = table["full_path"]
        in_destination = table["destination"]
        logging.info("Data table: " + str(in_name))
        logging.info("Input table source: " + str(in_destination))

        return in_name, in_name_1


def update_list(input_data):
    """
    Confirm if the list exist
    1. if not, create a new list
    2. if yes, update contact list if missing anything
    """

    """ confirm list existence """
    request = client.lists.all(get_all=True)

    ### verify if event exist in list
    status_bool = False
    event_id = ""
    new_request = ""
    for i in request['lists']:
        if str(i['name']) == input_data['group_name']:
            status_bool = True
            event_id = i['id']

    if not status_bool:
        ### JSON template to create a new list
        request_data_json = {
            "name":"",
            "contact":{
                "company":"ACETECH",
                "address1":"900-1188 West Georgia Street",
                "city":"Vancouver",
                "state":"BC",
                "zip":"V6E4A2",
                "country":"CA",
            },
            "campaign_defaults":{
                "from_name":"Leo",
                "from_email":"leo@keboola.com",
                "subject":"testing",
                "language":"en"
            },
            "permission_reminder":"You are receiving this email because you are a member of ACETECH or have attended an ACETECH event.",
            "email_type_option": False
        }
        ### Configure JSON parameters
        request_data_json['name']=input_data['group_name']
        request_data_json['campaign_defaults']['from_name'] = FROM_NAME
        request_data_json['campaign_defaults']['from_email'] = FROM_EMAIL

        new_request = client.lists.create(data=request_data_json)
        event_id = new_request['id']

    temp_bool = _update_members(event_id,input_data)
    status_bool = temp_bool and status_bool
    
    return event_id,status_bool


def _update_members(event_id,input_data):
    """
    updating the member list within the campaign list using the input data
    1. if found, ignore
    2. if not found in campaign list, add
    """
    status_bool = True
    ### adding member into the list
    ### JSON template for adding new member
    request_data_json = {
        "email_address":"",
        "status":"subscribed",
        "merge_fields":{
            "FNAME":"",
            "LNAME":""
        }
    }
    ### configure JSON parameters
    request_data_json['merge_fields']['FNAME'] = input_data['first_name']
    request_data_json['merge_fields']['LNAME'] = input_data['last_name']
    request_data_json['email_address'] = input_data['host']

    ### Create a new member if it does not exist in the list
    try:
        client.lists.members.create(list_id = event_id, data = request_data_json)
    except Exception as err:
        logging.error(err)
        status_bool = False

    return status_bool


def create_campaign(list_id,template_id,data_input,column_headers):
    """
    Create campaign for the event
    """
    ### JSON template for new campaign
    request_data_json = {
        "type":"regular",
        "recipients":{
            "list_id":""
        },
        "content_type":"template",
        "settings":{
            "subject_line":"",
            "title":"",
            "from_name":"Austin",
            "reply_to":"leo@keboola.com",
            "template_id":0
        }
    }
    ### Configure JSON parameters
    request_data_json['recipients']['list_id'] = list_id
    request_data_json['settings']['title'] = data_input['group_name']
    #request_data_json['settings']['subject_line'] = "Event Reminder: {0}".format(data_input['group_name'])
    request_data_json['settings']['subject_line'] = data_input['email_subject']
    request_data_json['settings']['from_name'] = FROM_NAME
    request_data_json['settings']['reply_to'] = FROM_EMAIL
    request_data_json['settings']['template_id'] = int(TEMPLATE_ID)

    ### Purpose: get template content
    new_request = client.campaigns.create(data=request_data_json)
    campaign_id = new_request['id']
    
    ### Creating email content
    email_content = _update_campaign_content(campaign_id,data_input,column_headers)
    request_data_json['content_type'] = 'html'
    request_data_json['settings']['template_id'] = 0
    client.campaigns.delete(campaign_id=campaign_id)

    ### Create new campaign
    new_request = client.campaigns.create(data=request_data_json)
    campaign_id = new_request['id']
    client.campaigns.content.update(campaign_id=campaign_id, data=email_content)
    
    return campaign_id
    

def send_campaign(campaign_id_list,campaign_status):
    """
    Sending campaign
    """
    for i in campaign_id_list:
        try:
            client.campaigns.actions.send(campaign_id = i)
            campaign_status.append(True)
        except Exception:
            campaign_status.append(False)
    return campaign_status


def _create_campaign_content(data_input):
    """
    Create content for campaign
    """
    ### JSON template for the new content
    request_data_json ={
        "html":" "
    }
    ### Replacing parameters
    request_data_json['html'] = EMAIL_CONTENT
    string_to_replace = request_data_json['html']
    string_to_replace = string_to_replace.replace('[FIRST_NAME]',data_input['first_name'])
    string_to_replace = string_to_replace.replace('[LAST_NAME]',data_input['last_name'])
    string_to_replace = string_to_replace.replace('[COMPANY]',data_input['company_name'])
    string_to_replace = string_to_replace.replace('[GROUP_NAME]',data_input['group_name'])
    string_to_replace = string_to_replace.replace('[EVENT_DATE]',data_input['event_date'])
    string_to_replace = string_to_replace.replace('[FROM_NAME]',FROM_NAME)
    request_data_json['html'] = string_to_replace
    
    return request_data_json


def _update_campaign_content(campaign_id,data_input,column_headers):
    """
    Update content in campaign
    """
    ### JSON template for the new content
    request_data_json ={
        "html":" "
    }
    ### Replacing parameters
    request = client.campaigns.content.get(campaign_id=campaign_id)

    ### Replacing html raw code
    request_data_json['html'] = request['html']
    string_to_replace = request_data_json['html']
    string_to_replace = string_to_replace.replace('*|FROM_NAME|*',FROM_NAME)
    string_to_replace = string_to_replace.replace('*|COMPANY_NAME|*',data_input['company_name'])
    for i in column_headers:
        temp = "*|"+i.upper()+"|*"
        string_to_replace = string_to_replace.replace(temp,str(data_input[i]))
    request_data_json['html'] = string_to_replace
    
    return request_data_json


def _delete_all_campaigns():
    """ 
    Deleting all campaigns
    """
    request = client.campaigns.all(get_all=True)
    campaign_id_list = []
    for i in request['campaigns']:
        campaign_id_list.append(i['id'])
        client.campaigns.delete(campaign_id = i['id'])
    logging.info("Campaigns Deleted: {0}".format(campaign_id_list))

    return


def _delete_all_lists():
    """ 
    Deleting all lists 
    """
    request = client.lists.all(get_all=True)
    listid_list = []
    for i in request['lists']:
        listid_list.append(i['id'])
        client.lists.delete(list_id=i['id'])
    logging.info("Lists Deleted: {0}".format(listid_list))

    return


def delete_all():
    """ 
    Deleting all campaigns and lists 
    """
    _delete_all_campaigns()
    _delete_all_lists()
    return


def _idle_list(listid_list):
    """
    Idling till the list has filled the recipient
    """
    time.sleep(30)
    if len(listid_list) == 0:
        return
    temp_list = listid_list
    for i in listid_list:
        request = client.lists.get(list_id=i)
        status = request['stats']['member_count']
        if status != 0:
            temp_list.remove(i)

    ### Recursion 
    _idle_list(temp_list)
    
    return


def _idle_campaign(campaign_id_list):
    """ 
    Idling till the campains are all sent out 
    """
    time.sleep(10)
    if len(campaign_id_list) == 0:
        return
    temp_campaign_list = campaign_id_list
    for i in campaign_id_list:
        request = client.campaigns.get(campaign_id=i)
        status = request["status"]
        if status == "sent":
            temp_campaign_list.remove(i)

    ### Recursion
    _idle_campaign(temp_campaign_list)

    return


def main():
    """ 
    Main Execution Script
    """
    ### import input table
    if TEMPLATE_UPDATE:
        input_table, input_table_1 = get_tables(in_tables) 
        data_1 = pd.read_csv(input_table_1,dtype=str) 
        template_headers = list(data_1)
    else:
        input_table = get_tables(in_tables)  

    data = pd.read_csv(input_table,dtype=str)
    
    ### output csv stating whether the email for that row is successful
    output = data
    ### column_headers
    column_headers = list(data)
    if TEMPLATE_UPDATE:
        ### if template_update is enabled:
        for i in template_headers:
            if i in column_headers:
                pass
            else:
                column_headers.append(i)
    
    ### PARAMS
    listid_list = []
    campaign_id_list = []
    campaign_list = []
    campaign_status = []
    bad_template_list = []

    ### updating the contact list per input table roll
    itr = 0 
    while itr < len(data):
        status_bool = True
        template_bool = True
        if TEMPLATE_UPDATE:
            if data['group_name'][itr] in list(data_1['group_name']):
                pass
            else:
                template_bool = False
                logging.error("Template for {0} cannot be found".format(data['group_name'][itr]))
                bad_template_list.append(data['group_name'][itr])
        
        if template_bool:
            ### validate if list creation is successful
            list_id, list_bool = update_list(data.iloc[itr])
            if list_id in listid_list:
                pass
            else:
                listid_list.append(list_id)
            
            ### validate if campaign creation was successful
            if data['group_name'][itr] in campaign_list or data['group_name'][itr] in bad_template_list:
                pass
            else:
                data_input = data.iloc[itr]
                if TEMPLATE_UPDATE:
                    ### Combining columns between input files and template config files
                    try:
                        template_index = data_1.loc[data_1['group_name']==data_input['group_name']].index[0]
                        logging.info("Template Configuration is loaded.")
                        data_input = pd.concat([data_input,data_1.iloc[template_index]])#,join='inner')
                        data_input = data_input.T.drop_duplicates().T
                    except Exception:
                        logging.error("Template for {0} cannot be found".format(data['group_name'][itr]))
                        bad_template_list.append(data['group_name'][itr])
                        status_bool  = False

                ### continue if no errors have been found prior campaign creations

                if status_bool:
                    campaign_id = create_campaign(list_id, TEMPLATE_ID,data_input,column_headers)
                    campaign_id_list.append(campaign_id)
                    campaign_list.append(data['group_name'][itr])
                else:
                    pass
            
        itr+=1
    
    ### idle till all the list is updated in campaign
    _idle_list(listid_list)
    ### send campaign & output file with status of the campaign
    campaign_status = send_campaign(campaign_id_list,campaign_status)
    ### idle till all the campaigns are sent out
    _idle_campaign(campaign_id_list)
    
    """
    ### Delete Campaigns and the lists to avoid duplicates 
    for i in campaign_id_list:
        client.campaigns.delete(campaign_id=i)
    for i in listid_list:
        client.lists.delete(list_id=i)
    for i in bad_listid_list:
        client.lists.delete(list_id=i)
    """
    ### Delete Campaigns and the lists to avoid duplicates 
    #_delete_all_lists()
    for i in listid_list:
        client.lists.delete(list_id=i)
        logging.info("Lists Deleted: {0}".format(i))

    ### Output files with status
    itr = 0
    output['status'] = 'sent'
    while itr < len(campaign_status):
        ### determine if campaign was sent out successfully
        if campaign_status[itr]:
            pass
        else:
            indexes = output.loc[data['group_name']==campaign_list[itr]].index
            for i in indexes:
                output['status'][i] = 'failed'
        itr += 1
    for i in bad_template_list:
        indexes = output.loc[data['group_name']==i].index
        for i in indexes:
            output['status'][i] = 'failed'

    ### Output file with status
    output.to_csv(DEFAULT_FILE_DESTINATION+"output.csv",index=False)

    return



if __name__ == "__main__":

    main()
    #delete_all()
    #_delete_all_campaigns()
    #_delete_all_lists()

    logging.info("Done.")
