# README # 
This repository is a collection of configurations which aid users to send out emails to multiple users through MailChimp.

## API Documentation ##
[MailChimp API documentation](http://developer.mailchimp.com/documentation/mailchimp/reference/overview/)

## Configuration ##
Required Information:

1. User Name
    - Username to login your MailChimp Account
2. API Key
    - API key to access your MailChimp Account (Account -> Extra -> API keys -> Create A Key)
3. From Name
    - The sender's display name in the emails being send out
4. From Email
    - The Email address which will be used to send out the campaign emails
    - ** In order to use the email address to send out emails, users will have to verfy the email domain in MailChimp (Account -> Settings -> Verified domains) **
5. Template ID
    - The template which will be used for the email automation
    - Template ID is a 5 digit numbers which can be obtained from the template URL
6. Template Update
    - Additional information which is required to insert into the template
    - When it is enabled, a template configuration is mandatory as the second input of the Input Mappings
    - Program will ignore the second input mapping if "Template Update" is *NOT* enabled

## Input File Format ##
### Participants' List ###
Required Information:  

1. first_name
    - Participant's first name
2. last_name
    - Participant's last name
3. host
    - Participant's Email address 
4. group_name
    - Name of the event; This will also be the name of the campaign and list in MailChimp  
5. email_subject
    - The email subject which will be used to send out the reminder/survey emails

Additional Information:  

- If users want to insert more inputs into the template, users are required to name the "variable to be replaced" (using all upper cases enclosed with "\*|" in the begining and 
    "|\*" in the rear) in the template identical to the input column names 
    - Example:
        1. Input file column: "column_header"
        2. template variable: "\*|COLUMN_HEADER|\*"

### Template Configuration ###

Required Information:

1. group_name
    - The variable used to match up with the newly created campaign's name

Additional Information:

- If users want to insert more inputs into the template, users are required to name the "variable to be replaced" (using all upper cases enclosed with "\*|" in the begining and 
    "|\*" in the rear) in the template identical to the input column names 
    - Example:
        1. Input file column: "column_header"
        2. template variable: "\*|COLUMN_HEADER|\*"


## Contact Info ##
Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  