# Terms and Conditions

The MailChimp Campaign Writer is built and offered by Leo as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to send out email contents across all the recipients with the given inputs via MailChimp API.
API call is process by using user-entered keys for API authentication, no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## Documentation
[MailChimp Campaign Writer Configuration](https://bitbucket.org/chanleoc/mailchimp_event_reminder/raw/fd925e35b583fdc298689be4b981e2b886a85f22/README.md)

## Contact

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  